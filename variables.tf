variable "prefix" {
  default = "gitlab-interview"
}

variable "region" {
  default = null
}

variable "ssh_public_key_file" {
  default = null
}

variable "external_ip_allocation" {
  default = null
}


