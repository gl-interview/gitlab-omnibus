terraform {
  backend "s3" {
    bucket = "tf-state-caleb"
    key    = "gitlab_interview"
    region = "us-east-2"
  }
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = var.region
}

