# 2k (The provided reference architecture in the take-home test image seems to match the 2k reference in the GitLab docs)
# https://docs.gitlab.com/ee/administration/reference_architectures/2k_users.html

# NOTE: When using the Docker tag 2 (mistakenly thought 2 was the latest) for the toolkit, I encountered an issue with AMIs using ARM architecture and switched to x86 instance types.
# It may be the case that the newer version (>=3) of the toolkit may support ARM.
# If I had more time, I might have experimented with ARM instance types as they are cheaper and have unlimited burst, but to limit the number of unknowns for this demo, I am sticking with the x86 architecture.

# Leaving other Terraform variable defaults alone for buckets and other resources, as it allows the destroy to work as expected. If this were a production GitLab instance, I would not allow buckets and other resources to be easily removed and would override the needed defaults.

module "gitlab_ref_arch_aws" {
  source = "../../../gitlab-environment-toolkit/terraform/modules/gitlab_ref_arch_aws"

  prefix         = var.prefix
  ssh_public_key = file(var.ssh_public_key_file)

  # For the purposes of this demo, the Gitaly, GitLab Rails, and the monitor seem to be fine for burstable instances for inconsistent/shorter workloads. If this were a production system, I would follow the 2k reference architecture and use the recommended instance types.
  # I am using large instance sizes because there were Ansible steps that took a long time with the small and micro sizes, notably the database migrations took >15 minutes. The large size seems to work well, and Ansible runs quickly enough.

  gitaly_node_count     = 1
  gitaly_instance_type  = "t3.large"
  monitor_node_count    = 1
  monitor_instance_type = "t3.large"

  gitlab_rails_node_count    = 3
  gitlab_rails_instance_type = "t3.large"

  # In theory at least, m series instance types should be more stable for heavier/consistent worklaods as they are not burstable
  redis_node_count    = 1
  redis_instance_type = "m5.large"

  postgres_node_count    = 1
  postgres_instance_type = "m5.large"

  create_network = true # Creating a new VPC dedicated to Gitlab, ensures no IP limitations/Security Groups/ACLs/other limitations with other instances and policies

  haproxy_external_node_count                = 1
  haproxy_external_instance_type             = "m5.large"
  haproxy_external_elastic_ip_allocation_ids = [var.external_ip_allocation]
}

output "gitlab_ref_arch_aws" {
  value = module.gitlab_ref_arch_aws
}

