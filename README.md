# gitlab-omnibus

Sucessful Pipeline run: https://gitlab.com/gl-interview/gitlab-omnibus/-/pipelines/1121902993 (Set up gitlab-ee)

NOTE: The prefix used for this environment is `gitlab-interview`

NOTE: There is some issue with the gitlab-ce build with ansible, but the infrastructure seems to run as expected. There also seems to be a 422 error on login that may be related to SSL not being enabled. 

EDIT: Post submission, It bothered me too much that this was blocking me, I decided to enable SSL, leverage letsencrypt, and duckdns

EDIT: It seems the aws_region not being set properly, I was able ot find the error via the `gitlab-ctl tail` command in the gitlab-rails instances 

EDIT: It seems the 422 was resolved with the redis configurations being set properly. It might have been the case the the multi-line ansible command in the CI pipeline was not properly setting extra vars. It seemed that the redis password was not being set as expected and the templates were rendered with an empty password value. Putting the command all in one line in the CI/CD seemed to resolve the issue. Re-reading through the advanced sections of the docs revealed there were a couple of required parameters missing for redis and posgres to render the gitlab-rail configurations to find the external redis and postgres nodes. Updating the ansible vars and changing the ansible command in the CI pipeline fixed these issues and it seems that Gitlab now behaves as expected

## How to run
1. On the left screen menu, Find **Build** and click to expand the section
1. Under **Build** click on **Pipelines**
1. On the right side of the screen click on **Run Pipeline**
1. Then click on **Run Pipeline** from the 'main' branch, no need to add variables
1. To Remove the infrastructure add a variable to a manual run named `DESTROY` with any value and a plan to remove the infrastructure will be generated and can be applied. This will also skip the ansible configuration job.

OR 

Push changes to this repo and a pipeline will be triggered automatically

### Execution
1. The first stage is `plan` that will trigger a `terraform plan`. This can be reviewed in the CI/CD output logs.
1. The next stage is `apply` this can only be executed be a manual trigger so a human can validate the plan before applying it. If the plan looks good from the previous stage then run the pipeline to execute `terraform apply`
1. The final stage is `configure` this will run if the `apply` stage is triggered, an `ansible-playbook` command will execute and apply the configurations for gitlab-omnibus


### CI/CD custom variables needed for this pipeline

`AWS_ACCESS_KEY_ID` (The aws access key id for the automation user)

`AWS_SECRET_ACCESS_KEY` (The aws access key secret for the automation user)

`AWS_IP_ID` (The allocation id of the Elastic IP)

`AWS_REGION` (The AWS region where the aws infrastructure is created)

`DOCKER_AUTH_CONFIG` (Used for docker login)

`GL_DNS` (DNS used for lets encrypt and GitLab configurations)

`GITLAB_PASSWORD` (Gitlab default password, used for every secret for this demo)

`SSL_ISSUER_EMAL` (Email used for the manager of the GitLab systems and letsencrypt)

`SSH_PRIVATE_KEY` (note this is a file type variable, used by ansible to configure instances)

`SSH_PUBLIC_KEY` (note this is a file type variable, used by ansible to configure instances)

### Local Execution
```

  docker run -it --rm -v $(pwd):/gitlab-environment-toolkit/dir1/dir2 -v ~/.aws:/root/.aws/ registry.gitlab.com/gitlab-org/gitlab-environment-toolkit:3.1.1 /bin/bash -c 'cd dir1/dir2/; rtx install jq  && terraform output --json | jq ".gitlab_ref_arch_aws.value.postgres.internal_addresses[]";'
```


